# ag-valdequint

Éléments cartographiques pour illustrer l'année 2021 lors de l'Assemblée Générale du 2 avril 2022 de l'association Valdequint.

![](screenshot-geojson.jpg)

## Cartographie web

Le contenu du [fichier `data.geojson`](data.geojson) est à coller sur [geojson.io](https://geojson.io).

**Légende** :

- marqueurs _verts_ : confiant sur l'emplacement
- marqueurs _orange_ : c'est "à peu près par là"
- marqueurs _rouge_ : je ne sais pas ce que je suis en train de faire (et c'est OK)

## Cartographie QGIS

## Cartographie papier
